#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Mirar i dir si un argument és un directori, link o file
# ------------------------------------
ERR_NARG=1
ERR_NFILE=2
# 1) si num args no és correcte plegar
if [ $# -ne 1 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 dir, file or link"
   exit $ERR_NARG
fi
# 2) si el tipus d'argument és incorrecte
if ! [ -d $1 -o -f $1 -o -L $1 ]
then
   echo "Error:  $1 no és un file, dir or link"
   echo "Usage: $0 dir, file or link"
   exit $ERR_NFILE
fi
# PROGRAMA
entrada=$1
if [ -d $entrada ]
then
   echo "L'entrada $entrada és un directori"
elif [ -f $entrada ]
then 
   echo "L'entrada $entrada és un file"
else
   echo "L'entrada $entrada és un link"
fi
exit 0
