#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Descripció: exemple bucle for 
# ------------------------------------

# llistar tots el logins numerats i ordenats
pos=1
llistat_login=$(cut -d: -f1 /etc/passwd | sort)
for element in $llistat_login
do
 echo "$pos: $element"
 ((pos++))
done
exit 0

# llistar numerats els noms dels fitxers del directori actiu
pos=1
llistat=$(ls)
for element in $llistat
do
 echo "$pos: $element"
 ((pos++))
done
exit 0

# mostrar arguments enumerats
# tot comptador te dos steps inicialitzacio i increment
pos=0
for arg in $*
do
  pos=$(($pos + 1))
  echo "$arg $pos" 
done
exit 0

# iterar per la llista d'arguments
# $@ expandeix els words encara que estigun encapsulats

for arg in "$@"
do
  echo $arg
done
exit 0

# iterar per la llista d'arguments
# les cometes s'interpreten com una sola cadena
for arg in "$*"
do
  echo $arg
done
exit 0

# iterar per la llista d'arguments

for arg in $*
do
  echo $arg
done
exit 0

# iterar pel valor d'una variable
llistat=$(ls)
for nom in $llistat
do
 echo $nom
done
exit 0

# iterar per un conjunt d'elements
for nom in "pere pau marta anna"
do
  echo "$nom"
done
exit 0

# iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
  echo "$nom"
done
exit 0
