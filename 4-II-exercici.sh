#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar stdin mostrant per stdout les línies numerades i en majúscules
# ------------------------------------------------------------------------
# 1) llegir stdin
comptador=0
while read -r line
do
# 2) comptar lineas i pasar a majuscules
  ((comptador++))
  majuscules=$(echo $line | tr '[:lower:]' '[:upper:]')
  echo "$comptador: $majuscules"
done
exit 0
