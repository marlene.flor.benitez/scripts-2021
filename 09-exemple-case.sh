# #! /bin/bash
# @Marlene Flor 
# HISX M01-Sistemes Operatius
# Febrer 2021
# Exemples CASE
# ------------------------------------
case $1 in
	"dl"|"dt"|"dc"|"dj"|"dv")
	  echo "$1 és laborable";;
	"ds"|"dm")
	  echo "$1 és festiu";;
	*)
	  echo "$1 no és un dia"
esac

exit 0

case $1 in
	[aeiou])
	 echo "$1 és una vocal";;
	[bcdfghjklmnñpqrstvwxyz])
	 echo "$1 és una consonant";;
	*)
	 echo "$1 és una altra cosa"
esac

exit 0

case $1 in
   "pere"|"pau"|"joan")
   echo "és un nen"
   ;;
   "marta"|"anna"|"julia")
   echo "és una nena"
   ;;
   *)
   "és indefinit"
esac
