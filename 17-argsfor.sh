#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Validar arguments amb opcions vàlides [-a -b -c -i -s -1]
# -------------------------------------------
# 1) validar que hi ha dos arguments
ERR_NARG=1
ERR_TARG=2
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 file dir"
   exit $ERR_NARG
fi
# 2) evaluar arguments
llista=$*
opcions=""
arguments=""
for arg in $llista
do
  case $arg in
    "-a"|"-b"|"-c"|"-i"|"-s"|"-1")
    # acumulador de cadenes
    opcions="$opcions $arg"
    ;;
    *)
    arguments="$arguments $arg"
    ;;
  esac
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit 0
