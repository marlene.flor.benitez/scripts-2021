#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar els arguments i mostrar per stdout només els de 4 o més caràcters.
# ----------------------------------------------------------------------------

# VERSIÓ 1

# 1) validar si nº arguments és correcte
ERR_NARG=1
if [ $# -lt 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 alguna cosa"
  exit $ERR_NARG
fi
# 2) mirar si els arguments ingresats tenen 4 o més caràcters
for arg in $*
do
  echo $arg | egrep -q '.{4,}'
  if [ $? -eq 0 ]
  then
    echo $arg
  fi
done
exit 0

# VERSIÓ 2

# 1) validar si nº arguments és correcte
ERR_NARG=1
if [ $# -lt 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 alguna cosa"
  exit $ERR_NARG
fi
# 2) mirar si els arguments ingresats tenen 4 o més caràcters
for arg in $*
do
  caracters=$(echo $arg | wc -c)
  # el wc -c sempre compta 1 caràcter més del que hi ha realment, per tant
  # si volem 4 caràcter (reals) o més hem d'augmentar 1 
  if [ $caracters -ge 5 ]
  then
    echo $arg
  fi
done
exit 0
