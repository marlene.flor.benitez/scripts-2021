#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# validar una llista d'arguments
# -------------------------------------------
# 1) validar que hi ha arguments correctes
ERR_NARG=1
ERR_TARG=2
if [ $# -lt 1 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 file dir"
   exit $ERR_NARG
fi
# 2) validar els arguments
opcions=""
arguments=""
file=""
num=""
anterior=""
while [ -n "$1" ]
do
    if [ $anterior = "-a" -o $anterior = "-c" -o $anterior = "-d" ]
    then
	if [ -f $1 ]
	then
          file="$file $anterior $1"
	elif [ 
	else
          opcions="$opcions $anterior"

    fi 
    anterior=$1
done
exit 0 
