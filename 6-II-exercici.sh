#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar per stdin linies d’entrada tipus “Tom Snyder” i 
# mostrar per stdout la línia en format → T. Snyder.
# ----------------------------------------------------------

# VERSIO 1

# 1) llegir stdin
while read -r line
do
# 2) si el format no es valid, error
  echo $line | grep -Evq '^[A-Z][a-z]+ [A-Z][a-z]+'
  if [ $? -eq 0 ]
  then 
    echo "Error: format no vàlid" >> /dev/stderr 
    echo "Usage: $0 Nom Cognom" >> /dev/stderr
# si es valid, pasar al altre format
  else
    primera_lletra="$(echo $line | cut -c1)"
    cognom="$(echo $line | sed 's/^[^ ]*//')"
    echo "$primera_lletra.$cognom"
    fi
done
exit 0

# VERSIO 2

# 1) llegir stdin
while read -r line
do
# 2) si el format no es valid, error
  echo $line | grep -Evq '^[A-Z][a-z]+ [A-Z][a-z]+'
  if [ $? -eq 0 ]
  then 
    echo "Error: format no vàlid" >> /dev/stderr 
    echo "Usage: $0 Nom Cognom" >> /dev/stderr
# si es valid, pasar al altre format
  else
    primera_lletra="$(echo $line | cut -c1)"
    cognom="$(echo $line | cut -d' ' -f2)"
    echo "$primera_lletra. $cognom"
    fi
done
exit 0

