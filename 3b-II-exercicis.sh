#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar arguments que són matricules: 
# b) stdout les que són vàlides, per stderr les no vàlides. Retorna 
# de status el número d’errors (de no vàlides)
# ---------------------------------------------------------------------
# 1) validar nº arguments
ERR_NARG=1
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 usuari[...]"
   exit $ERR_NARG
fi
# 2) per cada matricula validem
no_valides=0
for matricula in $*
do
  echo $matricula | grep -Eq '^[0-9]{4}-[A-Z]{3}$'
  if [ $? -eq 0 ]
  then
    echo $matricula
  else
    echo "Error: matricula $matricula no vàlida" >> /dev/stderr
    ((no_valides++))
  fi
done
exit $no_valides
