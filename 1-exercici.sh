#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Mostrar l’entrada estàndard numerant línia a línia
# -----------------------------------------------------------

# llegir l'entrada estàndar i comptar
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0
