#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar línia a línia l’entrada estàndard, si la línia té més de 60 
# caràcters la mostra, si no no
# ---------------------------------------------------------------------
# PROGRAMA
while read -r line
do
  longitud=$(echo $line | wc -c)
  if  [ $longitud -gt 60 ]
  then
    echo "$line"
  fi
done
exit 0
