#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Mirar si un argument és directori o no, si ho és ho llistem
# ------------------------------------
ERR_NARG=1
ERR_NDIR=2
# 1) si num args no és correcte plegar
if [ $# -ne 1 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 dir"
   exit $ERR_NARG
fi
# 2) si el tipus d'argument és incorrecte

if ! [ -d $1 ]
then
   echo "Error:  $1 no és un directori"
   echo "Usage: $0 dir"
   exit $ERR_NDIR
fi
# PROGRAMA
dir=$1
ls $dir
exit 0
