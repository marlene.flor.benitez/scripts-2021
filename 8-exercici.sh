#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Fer un programa que rep com a argument noms d’usuari, si existeixen
# en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout.
# Si no existeix el mostra per stderr
# ---------------------------------------------------------------------
# 1) validar nº arguments
ERR_NARG=1
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 usuari[...]"
   exit $ERR_NARG
fi
# 2) validar si el nom d'usuari existeix en el sistema
llista=$(cut -d: -f1 /etc/passwd)
for usuari in $*
do
  echo $llista | grep -wq $usuari
  # si no existeix mostra per stderr
  if [ $? -eq 0 ]
  then
    echo $usuari 
  else
    echo "Error: $usuari no existeix" >> /dev/stderr
  fi
done
exit 0
