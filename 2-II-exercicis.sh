#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar els arguments i comptar quantes n’hi ha de 3 o més caràcters.
# ----------------------------------------------------------------------------

# VERSIÓ 1

# 1) validar si nº arguments és correcte
ERR_NARG=1
if [ $# -lt 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 alguna cosa"
  exit $ERR_NARG
fi
# 2) mirar si els arguments ingresats tenen 3 o més caràcters
comptador=0
for arg in $*
do
  echo $arg | egrep -q '.{3,}'
  if [ $? -eq 0 ]
  then
    ((comptador++))
  fi
done
echo "Arguments amb 3 car o més: $comptador"
exit 0

# VERSIÓ 2

# 1) validar si nº arguments és correcte
ERR_NARG=1
if [ $# -lt 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 alguna cosa"
  exit $ERR_NARG
fi
# 2) mirar si els arguments ingresats tenen 3 o més caràcters
comptador=0
for arg in $*
do
  caracters=$(echo $arg | wc -c)
  # el wc -c sempre compta 1 caràcter més del que hi ha realment, per tant
  # si volem 3 caràcter (reals) o més hem d'augmentar 1 
  if [ $caracters -ge 4 ]
  then
    ((comptador++))
  fi
done
echo "Arguments amb 3 car o més: $comptador"
exit 0
