#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Exemples bucle while
# -----------------------------------------------

# 7) numerar stdin linea a linea i passar a majúscula
num=0
while read -r line
do
  ((num++))
  echo "$num: $line" | tr '[:lower:]' '[:upper:]'
done
exit 0

# 6) procesar stin fins al token FI
read -r line
while [ "$line" != "FI" ]
do	
  echo "$line"
  read -r line
done
exit 0

# 5) procesa stin i numera linea a linea
num=0
while read -r line
do
  ((num++))
  echo "$num: $line"
done
exit 0

# 4) processar l'entrada estàndard línea a línea
while read -r line
do
  echo $line
done
exit 0

# 3) iterar arguments amb shift
while [ -n "$1" ]
do
  echo "$1, $#, $*"
  shift
done
exit 0

# 2) compador decreixent del arg fins a 0
num=$1
MIN=0
while [ $num -ge $MIN ]
do
  echo -n "$num, "
  ((num--)) # disminuim 1 a la variable
done
exit 0

# 1) mostrar un comptador de 1 al MAX
MAX=10
comptador=1
while [ $comptador -le $MAX ]
do
   echo "$comptador"
   ((comptador++))
done
exit 0
