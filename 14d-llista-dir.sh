#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Mirar si un argument és directori o no, si ho és ho llistem i numerem
# ------------------------------------
ERR_NARG=1
ERR_NDIR=2
# 1) si num args no és correcte plegar
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 dir"
   exit $ERR_NARG
fi
# 2) si el tipus d'argument és incorrecte
if ! [ -d $1 ]
then
   echo "Error:  $1 no és un directori"
   echo "Usage: $0 dir"
   exit $ERR_NDIR
fi
# PROGRAMA

while [-m "$1"]
  dir=$1
  if ! [-d $dir ]
  do
   echo "Error:  $1 no és un directori" >> /dev/null
   echo "Usage: $0 dir" 
  else
    llista_dir=$(ls $dir)
    for arg in $llista_dir
    do
      if [ -L "$dir/$arg" ]
      then
         echo "L'entrada $arg és un link"
      elif [ -f "$dir/$arg" ]
      then 
         echo "L'entrada $arg és un file"
      elif [ -d "$dir/$arg" ]
      then
         echo "L'entrada $arg és un directori"
      else
         echo "L'entrada $arg és un altre cosa"
      fi
    done
   fi
 done
exit 0
