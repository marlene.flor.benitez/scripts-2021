# Descripció: rep almenys una nota o mes
# per cada nota diu si suspés, aprovat, notable, excelent
# ------------------------------------
# 1) si num args no és correcte plegar
if [ $# -lt 1 ] # menys de 1 = -eq 0
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit $ERR_NARG
fi
# PROGRAMA
for nota in $*
 if ! [ $nota -ge 0 -a $nota -l 10 ]
 then
   echo "Error: $nota no vàlida [0-10]" >> /dev/null
 elif [ $nota -lt 5 ]
 then
   echo "Nota $nota Suspès"
 elif [ $nota -lt 7 ]
 then
   echo "Nota $nota Aprovat"
 elif [ $nota -lt 9 ]
 then
   echo "Nota $nota Notable"
 else
   echo "Nota $nota Excel·lent"
 fi
done

