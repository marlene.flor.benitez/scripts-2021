#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar arguments que són matricules: 
# a) llistar les vàlides, del tipus: 9999-AAA.
# ---------------------------------------------------------------------
# 1) validar nº arguments
ERR_NARG=1
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 usuari[...]"
   exit $ERR_NARG
fi
# 2) per cada matricula validem
for matricula in $*
do
  echo $matricula | grep -Eq '^[0-9]{4}-[A-Z]{3}$'
  if [ $? -eq 0 ]
  then
    echo $matricula
  fi
done
exit 0
