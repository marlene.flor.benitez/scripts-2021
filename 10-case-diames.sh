#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Descripció: dir quants dies te un mes 
# ------------------------------------
ERR_NARG=1
ERR_RANG=2
# 1) si num args no és correcte plegar
if [ $# -ne 1 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit $ERR_NARG
fi
# 2) si el rang de l'entrada és incorrecta
if ! [ $1 -le 12 -a $1 -ge 1 ]
then
   echo "Error:  $1 no és un mes vàlid"
   echo "mes pren valors de 1 al 12"
   echo "Usage: $0 mes"
   exit $ERR_RANG
fi
# PROGRAMA
mes=$1
case $mes in
   "1"|"3"|"5"|"7"|"8"|"10"|"12")
   echo "El mes $mes té 31 dies"
   ;;
   "4"|"6"|"9"|"11")
   echo "El mes $mes té 30 dies"
   ;;
   "2")
   echo "El mes $mes té 28 dies"
esac
exit 0
