#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Copiar un file(origen) a un dir(destí)
# -------------------------------------------
# 1) validar que hi ha dos arguments
ERR_NARG=1
ERR_TARG=2
if [ $# -lt 2 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 file dir"
   exit $ERR_NARG
fi
# 2) primer argument és un file
origen=$1
if ! [ -f $origen ]
then
  echo "Error: $origen no és un file"
  echo "Usage: $0 file dir"
  exit $ERR_TARG
fi
# 3) segon és un directori
desti=$2
if ! [ -d $desti ]
then
  echo "Error: $desti no és un dir"
  echo "Usage: $0 file dir"
  exit $ERR_TARG
fi
# PROGRAMA - fileorigen a dirdesti
cp $file $desti
exit 0
