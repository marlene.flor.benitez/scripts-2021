#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Mostrar línia a línia l’entrada estàndard, retallant només els 
# primers 50 caràcters.
# --------------------------------------------------------------
# PROGRAMA
while read -r line
do
  echo $line | cut -c-50
done
exit 0
