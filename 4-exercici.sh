#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Fer un programa que rep com a arguments números de més (un o més) i indica
# per a cada mes rebut quants dies té el més.
# ------------------------------------------------------------
ERR_NARG=1
# 1) si num args no és correcte plegar
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit $ERR_NARG
fi
# PROGRAMA
for mes in $*
do
  # si el mes introduit es incorrecte
  if ! [ $mes -ge 1 -a $mes -le 12 ]
  then
     echo "Error:  $mes no és un mes vàlid" >> /dev/stderr 
  # si no ho és
  else
     case $mes in 
	  "2")
	  dies=28;;
          "4"|"6"|"9"|"11")
          dies=30;;
          *)
	  dies=31
     esac
  fi
  echo "El mes $mes té $dies dies"
done
exit 0
