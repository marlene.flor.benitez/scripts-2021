#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Mostar els arguments rebuts línia a línia, tot numerànt-los.
# ------------------------------------------------------------
ERR_NARG=1
# 1) si num args no és correcte plegar
if [ $# -lt 1 ] # menys de 1 = -eq 0
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 alguna cosa menos nada"
   exit $ERR_NARG
fi

# PROGRAMA
num=1
for arg in $*
do
  echo "$num: $arg"
  ((num++))
done
exit 0
