#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# crear un o mes directoris 
# -------------------------------------------
ERR_NARG=1
ERR_NoDIR=2
TOT_OK=0
sortida=0
if  [ $# -lt 1 ]
then
  echo "Error: Número d'arguments incorrecte"
  echo "Usage: $0 dir[...]"
  exit $ERR_NARG
fi
# per cada ruta indicada crear un directori
for ruta in $*
do
  mkdir $ruta &> /dev/null
  # mirem si el directori s'ha pogut crear
  resultat=$(echo $?)
  if [ $resultat -eq 0 ]
  then
    sortida=$TOT_OK
  else
    sortida=$ERR_NoDIR
  fi
done
# mostrem un missatge o altre depenent de la sortida
echo $sortida
if [ $sortida -eq $TOT_OK ]
then
  echo "Tots els directoris han estat creats"
else
  echo "Algun directori no s'ha pogut crear"
fi
exit 0
