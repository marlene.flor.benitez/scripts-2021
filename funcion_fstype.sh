##! /bin/bash
# @MarleneFlor
# 1HISX M01-Sistemes Operatius
# Exercicis funcions fstype
# ------------------------------------------------------
# 6) fstype fstype (ext4, ntfs, etc)
# Donat un fstype d'argument llista el device i el mountpoint 
# (per odre de device) de les entrades de fstab d'aquest fstype.

function fstype(){
	# validar que rep un arg
	if [ $# -ne 1 ] 
	then
	  echo "Error: numero d'arguments incorrecte"
	  echo "Usage: $0 fstype"
	  return 1
	fi
	# filtrar les lineas de fitxer que pertanyes a el fstype
	fs=$1
	fileIn=myfstab
	while read -r line
	do
	  tipus=$(echo $line | cut -d' ' -f3)
	  if [ $tipus = $fs ] 
	  then
	    echo "DEVICE: $(echo $line | cut -d' ' -f1) MOUNTPOINT: $(echo $line | cut -d' ' -f2)"
          fi
	done < $fileIn
}

# 7) allfstype 
# LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) 
# les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.

function allfstype(){
	fstypes=$(cut -d' ' -f3 myfstab | sort -u)
	for type in $fstypes
	do
	  fstype $type
	done
}

# 8) allfstypeif 
# LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) 
# les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
# Es rep un valor numèric d'argument que indica el numéro mínim d'entrades d'aquest fstype que hi ha d'haver per sortir al llistat.

function allfstypeif(){
	
}
