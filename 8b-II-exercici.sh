# ! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Programa: prog file…
# b) Validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s’ha comprimit correctament, o un missatge d’error 
# per stderror si no s’ha pogut comprimir.
# En finalitzar es mostra per stdout quants files ha comprimit. 
#   Retorna status 0 ok, 1 error nº args, 2 si algun error en comprimir.
#   Ampliat per -h, --help
# ------------------------------------------------------------------------------------
status=0
ERR_NARG=1
# 1) validar nº arguments
if [ $# -eq 0 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 file[...]"
  exit $ERR_NARG
fi
# 2) comprimir cada file
if [ $1 = "-h" -o $1 = "--help" ]
then 
  echo "Usage: $0 file [...]"
  echo "Els arguments a ingresar han de ser fitxers, poden ser de qualsevol tipus sempre i quan siguin fitxers no comprimits"
else
comprimits=0
  for file in $*
  do
    # validar si és un file
    if [ -f $file ]
    then
      # comprimir
      gzip $file
      if [ $? -eq 0 ]
      then
        echo "$file.gz"
        ((comprimits++))
      else
        echo "Error: $file no s'ha pogut comprimir" >> /dev/stderr
        status=2
      fi
    # si no ho és error
    else
      echo "Error: $file no s'ha pogut comprimir" >> /dev/stderr
      status=2
    fi
  done
fi
echo "Comprimits $comprimits/$#"
exit $status
