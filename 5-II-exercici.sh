#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Processar stdin mostrant per stdout les línies de menys de 50 caràcters.
# ------------------------------------------------------------------------
# 1) llegir stdin
while read -r line
do
# 2) filtrar lineas amb menys de 50 car
  echo $line | grep -Evq '.{50,}'
  if [ $? -eq 0 ]
  then
    echo $line
  fi
done
exit 0
