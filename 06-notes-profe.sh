#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Validar nota: suspès, aprovat, notable, excelent
#  Una nota està suspesa si és inferior a un 5, aprovat si és un 5 o un 6,
#  un notable si és entre un 7 i un 8 i excel·lent si està entre un 9 i un 10.
# ------------------------------------
ERR_NARG=1
ERR_RANG=2
# 1) si num args no és correcte plegar
if [ $# -ne 1 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit $ERR_NARG
fi
# 2) validar rang de notes
nota=$1
if ! [ $nota -le 10 -a $nota -ge 0 ]
then
   echo "Error: valor $nota fora del rang permés"
   echo "nota pren valors de 0 a 10"
   echo "Usage: $0 nota"
   exit $ERR_RANG
fi
# PROGRAMA
nota=$1
if [ $nota -lt 5 ]
then
  echo "La nota $nota és un Suspès"
elif [ $nota -lt 7 ]
then
  echo "la nota $nota és un Aprovat"
elif [ $nota -lt 9 ]
then
  echo "la nota $nota és un Notable"
else 
  echo "La nota $nota és un Excel·lent"
fi
exit 0
