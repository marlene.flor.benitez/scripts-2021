# ! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Programa: prog -f|-d arg1 arg2 arg3 arg4
# a) Valida que els quatre arguments rebuts són tots del tipus que indica el
# flag. És a dir, si es crida amb -f valida que tots quatre són file.
# Si es crida amb -d valida que tots quatre són directoris.
#    Retorna 0 ok, 1 error nº args, 2 hi ha elements errònis.
#    Ampliat per -h, --help
# ---------------------------------------------------------------------------------
status=0
ERR_NARG=1
# 1) validar nº arguments
if [ "$#" -ne 5 -a $# -ne 1 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi
# 2) separar opció d'arguments
opcio=$1
shift
# 3) validar opcio correcta
if [ "$opcio" != "-f" -a "$opcio" != "-d" -a "$opcio" != "-h" -a "$opcio" != "--help" ]
then
  echo "Error: opcio $opcio no vàlida"
  echo "Usage: $0 -f|-d arg1 arg2 arg3 arg4"
  status=2
  exit $status
fi
# 4) mirar opcio i validar args
if [ "$opcio" = "-h" -o "$opcio" = "--help" ]
then
  echo "Usage: $0 -f|-d arg1 arg2 arg3 arg4"
  echo "El primer valor a ingresar és l'opció pot ser -d (opcio per directori) o -f (opcio per fitxer), seguit de 4 arguments que es verificaràn si correspón a l'opció del primer valo -d (opcio per directori) o -f (opcio per fitxer), seguit de 4 arguments que es verificaràn si correspón a l'opció del primer valor"
else
  for arg in $*
  do
    if ! [ $opcio $arg ]
    then
      status=2
    fi
  done
fi
exit $status
