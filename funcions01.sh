#! /bin/bash
# @MarleneFlor
# 1HISX M01-Sistemes Operatius
# Exemples funcions
# -------------------------------------------------
function hola(){
  echo "hola"
  return 0
}

function dia(){
  date
  return 0
}

function suma(){
  echo $(($1 + $2))
  return 0
}

function all(){
 hola
 echo "avui som: $(dia)"
 suma 6 9
}

# (1) fsize
# donat un login, calcular amb du l'ocupacio
# del home de l'usuari. Cal obtenir el home del /etc/passwd

function fsize(){
	# 1) validar que existeix el login
	login=$1
	linea=$(grep "^$login:" /etc/passwd)
	if [ -z "$linea" ]
	then
	  echo "Error: user $login no existeix"
	  return 1
	fi
	# 2) obtenir el seu homeDir
	homeDir=$(echo $linea | cut -d: -f6)
	# 3) calcular el du -sh del homeDir
	du -sh $homeDir 2> /dev/null
	return 0
}

# 2) rep logins i per cada login es mostra l'ocupació de disc
# del home del usuari usanf fsize

function loginArgs(){
	# 1) validar al menys es rep un login
	if [ $# -eq 0 ]
	then
	  echo "Error: ha d'haver almenys un argument"
	  return 1
	fi
	# 2) iterar per cada login
        for login in $*
	do
	  fsize $login
	done
	#   2a) per cada login aplicar fsize
}

# 3) loginFile
# rep com argument un fitxer que conté logins linea per linea
# mostra l'ocupació de disc del home de cada user.

function loginFile(){
	# validar que existeix un argument que sigui file
	if [ ! -f "$1" ]
	then
	  echo "Error: fitxer inexistent"
	  return 1
	fi
	# procesar contingut del fitxer
	fileIn=$1
	while read -r login
	do 
	  fsize $login
	done < $fileIn
}

# 4) loginBoth
# loginBoth [file]
# procesa file o stdin
# S'HA DE FER UN SOL BUCLE NO VAL FER IFS :(
function loginBoth(){
	# per defecte procesa l'entrada estandar 
	fiileIn=/dev/stdin
	if [ $# -eq 1 ]
	then
	  fileIn=$1
	fi
	while read -r login
	do
          fsize $login 
        done < $fileIn
}

# 5) grepgid gid
# validar rep 1 arg
# validar gid valid
# retorna la llista de logins que tenen aquest grup com a grup principal

function grepgid(){
	# validar que rep 1 gid
        if [ $# -ne 1 ]
        then
	echo "Error: numero d'arguments incorrecte"
	  return 1
	fi
	# validar gid
	gid=$1
	grep -q "^[^:]*:[^:]*:$GID:" /etc/group
        if [ $? -ne 0 ]
	then
	 echo "Error: gid $gid no existeix"
	 return 2
	fi
	# llista de logins
	cut -d: -f1,4 /etc/passwd | grep ":$gid$" | cut -d: -f1
} 

# 6) gidsize gid
# donat un GID com a argument, mostrar per cadsa usuari que pertany
# a aquest grup l'ocupació de disc del seu home

function gidsize(){
	# validar que rep 1 arg
 	if [ $# -ne 1 ]
        then
	echo "Error: numero d'arguments incorrecte"
	  return 1
	fi
	# fer la llista de logins
	gid=$1
	llista_logins=$(grepgid $gid)
	# per cada login calculem el size
	loginArgs $llista_logins
}

# 7) allgidsize
# per cada gid del sistema calcular l'ocupacio del home
# dels seus usuaris

function allgidsize(){
	all_gid=$(cut -d: -f4 /etc/passwd | sort -nu)
	for gid in $all_gid
	do
	  echo "GID: $gid"
	  gidsize $gid
	done
}

# 8) filterGroup
# llistar les lineas del /etc/passwd dels users dels grups del 0 al 100

function filterGroup(){
	fileIn=/etc/passwd
	while read -r line
	do
	  gid=$(echo $line | cut -d: -f4)
	  if [ $gid -le 100 -a $gid -ge 0 ]
          then
	    echo $line
          fi
	done < $fileIn
}
