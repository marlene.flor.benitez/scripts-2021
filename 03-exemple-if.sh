#! /bin/bash
# @Marlene Flor 
# HISX M01-Sistemes Operatius
# Febrer 2021
# Exemple de estructures condicionals
# Exemple if: indica si és major d'edat
#	$ prog edat
# ---------------------------------------------
# 1) validem arguments
if [ $edat -gt 18]
then
	echo 'major'
else
	echo 'menor'
fi
