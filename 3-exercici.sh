#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Fer un comptador des de zero fins al valor indicat per l’argument rebut
# -----------------------------------------------------------------------
ERR_NARG=1
# 1) si num args no és correcte plegar
if [ $# -ne 1 ] 
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 numero"
   exit $ERR_NARG
fi
# PROGRAMA
NUM=$1
min=0
while [ $min -le $NUM ]
do
  echo -n "$min, "
  ((min++))
done
exit 0
