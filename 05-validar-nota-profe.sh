#! /bin/bash
# @Marlene Flor 
# HISX M01-Sistemes Operatius
# Febrer 2021
# Validar nota: suspés, aprovat
# ------------------------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) si num args no és correcte plegar
if [ $# -ne 1 ]
then
    echo "Error: número d'arguments incorrecte"
    echo "Usage: $0 nota"
    exit $ERR_ARGS
fi

# 2) validar rang de nota
nota=$1
if ! [ $nota -ge 0 -a $nota -le 10 ]
then
    echo "Error: valor de nota $nota no vàlid"
    echo "nota pren valors de 0 a 10"
    echo "Usage: $0 nota"
    exit $ERR_NOTA
fi
# xixa
