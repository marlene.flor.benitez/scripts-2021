#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Copiar un file(origen) a un dir(destí)
# -------------------------------------------
# 1) validar que hi ha dos o més arguments
ERR_NARG=1
ERR_TARG=2
if [ $# -lt 2 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 file dir"
   exit $ERR_NARG
fi
# 2) separar files i dir
desti=$(echo "$#")
# llista_files=$(echo $* | sed 's/ [^ ]*$//')
llista_files=$(echo $* | cut -d' ' -f-$(($# - 1)))
exit 0
# 3) validar si desti es un dir existent
desti=$2
if ! [ -d $desti ]
then
  echo "Error: $desti no és un dir"
  echo "Usage: $0 file dir"
  exit $ERR_TARG
fi
# 4) validar si els elements de la llista son files
for $file in $llista_files
do
  if ! [ -f $file ]
  then
     echo "Error: $file no és un file" >> /dev/stderr 
     echo "Usage: $0 file[...] dir" >> /dev/stderr
  else
    cp $file $desti
exit 0
