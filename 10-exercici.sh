#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Fer un programa que rep com a argument un número indicatiu del número màxim 
# de línies a mostrar. El programa processa stdin línia a línia i mostra
# numerades un màxim de num línies.
# ---------------------------------------------------------------------
ERR_NARG=1
# 1) validar si nº arguments és correcte
if [ $# -ne 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 numero"
  exit $ERR_NARG
fi
# PROGRAMA
MAXIM=$1
comptador=0
# llegim linea a linea
while read -r line
do
  ((comptador++))
  echo "$comptador: $line"
  # si el comptador arriba al maxim sortim
  if [ $comptador -eq $MAXIM ]
  then
    exit 0
  fi
done
