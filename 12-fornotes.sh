#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Descripció: rep almenys una nota o mes
# per cada nota diu si suspés, aprovat, notable, excelent
# ------------------------------------
ERR_NARG=1
ERR_RANG=2
# 1) si num args no és correcte plegar
if [ $# -lt 1 ] # menys de 1 = -eq 0
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 nota"
   exit $ERR_NARG
fi

# PROGRAMA
for nota in $*
do
  # validar rang de notes
  if ! [ $nota -le 10 -a $nota -ge 0 ]
  then
     echo "Error: valor $nota fora del rang permés"
     echo "nota pren valors de 0 a 10"
     echo "Usage: $0 nota"
  else
  # evaluar la nota
    if [ $nota -lt 5 ]
    then
      echo "La nota $nota és un Suspès"
    elif [ $nota -lt 7 ]
    then
      echo "la nota $nota és un Aprovat"
    elif [ $nota -lt 9 ]
    then
      echo "la nota $nota és un Notable"
    else 
      echo "La nota $nota és un Excel·lent"
    fi
  fi
done
exit 0
