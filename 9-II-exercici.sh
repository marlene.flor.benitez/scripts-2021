# ! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Programa: prog.sh [ -r -m -c cognom  -j  -e edat ]  arg…
#   Escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
#   No cal validar ni mostrar res!
#   Per exemple si es crida: $ prog.sh -e 18 -r -c puig -j wheel postgres ldap
#   retorna:     opcions «-r -j», cognom «puig», edat «18», 
#   arguments «wheel postgres ldap»
# -----------------------------------------------------------------------------------
# 1) validar nº arguments
ERR_NARG=1
if [ $# -lt 1 ]
then
  echo "Error: número d'arguments incorrecte"
  echo "Usage: $0 [ -r -m -c cognom  -j  -e edat ]  arg…"
  exit $ERR_NARG
fi
# 2) processar cada arg i classificar
arguments=""
opcions=""
cognom=""
edat=""
while [ $1 ]
do
  case $1 in
    "-e")
    edat=$2
    shift;;
    "-c")
    cognom=$2
    shift;;
    "-r"|"-m"|"-j")
    opcions="$opcions $1";;
    *)
    arguments="$arguments $1"
  esac
  shift
done
echo "Opcions: $opcions, Arguments: $arguments, Cognom: $cognom, Edat: $edat"
exit 0
