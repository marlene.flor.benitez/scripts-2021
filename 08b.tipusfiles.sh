#! /bin/bash
# @MarleneFlor 
# 1HISX M01-Sistemes Operatius
# Febrer 2021
# Mirar i dir si un argument és un directori, link o file
# ------------------------------------
ERR_NARG=1
ERR_NEXIST=2
# 1) si num args no és correcte plegar
if [ $# -ne 1 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 dir, file or link"
   exit $ERR_NARG
fi
# PROGRAMA
entrada=$1
 # primer mira si el arxiu existeix
if ! [ -e $entrada ]
then 
   echo "$entrada no existeix"
   exit $ERR_NEXIST
 # mira si es un link, perquè si es mira després té priorita el directori
elif [ -h $entrada ]
then
   echo "L'entrada $entrada és un link"
elif [ -f $entrada ]
then 
   echo "L'entrada $entrada és un file"
elif [ -d $entrada ]
then
   echo "L'entrada $entrada és un directori"
else
   echo "L'entrada $entrada no és una altra cosa"
fi
exit 0
