#! /bin/bash
# @Marlene Flor 
# 1HISX M01-Sistemes Operatius
# Febrer 2022
# Fer un programa que rep com a arguments noms de dies de la setmana i mostra
# quants dies eren laborables i quants festius.
# Si l’argument no és un dia de la setmana genera un error per stderr.
# ----------------------------------------------------------------------
ERR_NARG=1
# 1) si num args no és correcte plegar
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit $ERR_NARG
fi
# PROGRAMA
laborables=0
festius=0
for dia in $*
do
  case $dia in 
       "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
       ((laborables++));;
       "dissabte"|"diumenge")
       ((festius++));;
       *)
       echo "Error: el dia $dia no és un dia de la setmana" >> /dev/stderr
     esac
done
echo "Hi havien $laborables dies laborables i $festius dies festius"
exit 0
